## Team 03 Backlog

<img src="https://cdn.80000hours.org/wp-content/uploads/2012/07/home-16personalities.png" alt="drawing" width="300"/>

#### Fixed Meetings:

>Monday 10:00-12:00

>Thursday 13:00-15:00

#### Code Execution

1) **set-up your path** : cd to `team03\web-app\mbti_web` 
2) **run the server** : run the command `python manage.py runserver`
3) **use the website** : open your browser and go to `http://localhost:8000/`

<img src="https://i.chzbgr.com/full/7085677824/h1576BCBE/" alt="drawing" width="250"/>

##### Support Packages
_Model_
* tensorflow
* keras
* joblib
* pickle

_Pipeline Validator_
* goodtables
* tableschema
* pprint
* shutil
* os
* warnings
* numpy
* pandas
* pandas_schema

_Database Communicator_
* sqlite3
* pandas

_Data Cleaner_
* pandas
* re
* pkg_resources


#### Week 01: 

Status | Tasks | Description | Assigned Member/s 
 ----------- | ----------- | -----------| -----------
✔|Decide on an idea | **Plan A:** Personality Type Prediction , **Plan B:** Game Sales and Rating Prediction | All members via Brainstorming 
✔| Find Dataset | [Plan A Dataset](https://www.kaggle.com/datasnaek/mbti-type) , [Plan B Dataset](https://www.kaggle.com/rush4ratio/video-game-sales-with-ratings)| All members 
✔|Create a spreadsheet to track the attendence|[Link to spreadsheet](https://docs.google.com/spreadsheets/d/1jiVYiLEnnTibaqiNr5O3OOsfLiW31Jh5sAK0R-dw-js/edit?usp=sharing)| Salvatore 
✔|Brainstorm the requirements |...|All members
✔|Push the markdown for week 01 | Make it fancy <img src=https://octodex.github.com/images/minion.png alt="minion" width="45"> | Ranim
✔|Started with the final report |... |All members

-----------------------
#### Week 02: 
Status | Tasks | Description | Assigned Member/s 
 ----------- | ----------- | -----------| -----------
✔| Decide whether to stick to plan A or plan B| conclusion: decided on Personality type prediction | 
✔| Follow-up meeting with Riccardo | Friday 15th of Nov | 
✔| Divide tasks for next week ||
✔| Data cleaning | removing links, weird words, posts with less than 3 words ... etc| Salvatore and Hakim 
✔| Tokenization and Lemmatization | tokenize all posts and use a normalization technique (either stemming or lemmatization) | Ranim
✔| Embedding | Expirement BERT <img src=https://www.clipartwiki.com/clipimg/detail/220-2207149_bert-enjoys-reading-collecting-bottlecaps-and-eating-sesame.png alt="minion" width="40"> , or use an alternative| Ranim
✔| Create an initial CNN | ... | Duy
-----------------------
#### Week 03: 

Status | Tasks | Description | Assigned Member/s 
 ----------- | ----------- | -----------| -----------
✔| Basic UI for user| where you can input a sentence and ouput a prediction | Ranim
✔| Basic UI for admin| where you can update the dataset and clean it | Ranim
✔| Connect UI to backend| | Ranim 
✔| Research & setup django backend | | Hakim
✔| Deploy Model |  | Duy
✔| add database and DB Communicator component | | Salvatore
✔| Define or design a pipeline/architecture | | Salvatore
✔| Add Database to project | | Salvatore

--------------------
#### Week 04: 

Status | Tasks | Description | Assigned Member/s 
 ----------- | ----------- | -----------| -----------
 ✔|Resubmit assignment 1|...| Everyone!!
 ✔| Log in UI| Frontend for log in| Ranim
 X|Log in Logic| Decided not to have log in anymore | Ranim
 X|Add some JS to the website|has been omitted due to django| Salvatore and Ranim
 ✔|Implement training validation pipeline||Salvatore
 ✔|Implement serving validation pipeline ||Salvatore
 ✔|Modifications to cleaning process||Hakim and Salvatore
 ✔|Feature Engineering|Add POS tagging to lemmatization, fix tokenizer, TF-IDF, TSVD and Chi2Test for tabular data for Logistic Regression model| Duy
 ✔|compare CNN and Logistic regression and hybrid model|...| Duy
--------------------
#### Week 05: 

Status | Tasks | Description | Assigned Member/s 
 ----------- | ----------- | -----------| -----------
 ✔|Unit Testing on validation functions and data validation|| Ranim
 ✔|Dynamic training |...|Hakim
 ✔|Incorporate logic for traceability into the system.|...|Hakim
 ✔|increase the performance of the dynamic training|...|Duy
 ✔|set up User value proposition data|...|Salvatore

--------------------
#### Week 06: 

Status | Tasks | Description | Assigned Member/s 
 ----------- | ----------- | -----------| -----------
 ✔|User value proposition after Prediction|display percentages of the classifiers| Ranim
 ✔|Visualize the model evaluation||Duy 
 ✔|Write inference model related external packages/requirements/constraints|| Duy
 ✔|Adding functionality for retrieving and restoring evaluation|| Hakim
 ✔|Bug fixing|| Hakim
 ✔|Modularize the prediction page|| Salvatore
 
--------------------
#### Post-Fair: 

Status | Tasks | Description | Assigned Member/s 
 ----------- | ----------- | -----------| -----------
 ✔|Improve admin UI for invalid training data| Give a red flag to show which data is invalid, why or some more information| Ranim
 ✔|Model versioning |Ability to re-load old model(s)|Duy
 ✔|Modifications to pipelines validator|Improves error tracking for invalid CSV from the admin panel|Salvatore
 ✔|Visualize the model||Duy

 
--------------------

#### Notes:

    - Ranim was away in week 04